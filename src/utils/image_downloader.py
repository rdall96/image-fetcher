# image_downloader.py
# Copyright (c) 2020 Ricky Dall'Armellina (rdall96). All Rights Reserved.

import sys
from pathlib import Path
import time
from selenium import webdriver
import wget
from concurrent.futures import ProcessPoolExecutor
import os

from .download_stat import DownloadStat

# URL to search images at (Google images)
_SEARCH_URL = "https://www.google.com/search?safe=off&site=&tbm=isch&source=hp&q={q}&oq={q}&gs_l=img"
# Path to Chrome web driver for selenium to load interface
_DRIVER_PATH = os.path.join(
    Path().absolute().parent, "bin/chromedriver", sys.platform,
    f"chromedriver{('', '.exe')[sys.platform == 'windows']}"
)
# Max number of parallel image downloads
_MAX_CONCURRENT_DOWNLOADS: int = os.cpu_count() * 2
# Image download timeout
_DOWNLOAD_TIMEOUT_SECONDS: int = 30

class ImageDownloader:

    def __init__(self, query: str):
        """
        Creates a image downloader for a given image query
        :param query: Image search criteria (i.e.: pizza)
        """
        self.query = query

    # Modified from: https://towardsdatascience.com/image-scraping-with-python-a96feda8af2d
    def _fetch_image_urls(self, max_links_to_fetch: int, sleep_between_interactions: int = 1):
        """
        Load the web driver to start an instance of selenium and scrape the web for images
        matching the query
        :param max_links_to_fetch: maximum number of images url to fetch
        :param sleep_between_interactions: sleep timer for images to load
        :return: list, image urls
        """

        def scroll_to_end(wd: webdriver):
            """ Helper method to scroll the page """
            wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            time.sleep(sleep_between_interactions)

        # Build the url
        search_url: str = _SEARCH_URL.format(q=self.query)
        # Search results
        image_urls = set()
        image_count = 0
        results_start = 0

        # Load web driver
        with webdriver.Chrome(_DRIVER_PATH) as wd:
            # Load the page
            wd.get(search_url)
            # Fetch all possible images
            while image_count < max_links_to_fetch:
                scroll_to_end(wd)
                # Get all image thumbnail results
                thumbnail_results = wd.find_elements_by_css_selector("img.Q4LuWd")
                number_results = len(thumbnail_results)
                # Try to click every thumbnail such that we can get the real image behind it
                for img in thumbnail_results[results_start:number_results]:
                    try:
                        img.click()
                        time.sleep(sleep_between_interactions)
                    except Exception:
                        continue
                    # Extract image urls
                    actual_images = wd.find_elements_by_css_selector('img.n3VNCb')
                    for actual_image in actual_images:
                        if actual_image.get_attribute('src') \
                                and 'http' in actual_image.get_attribute('src'):
                            image_urls.add(actual_image.get_attribute('src'))

                    # Refresh image count
                    image_count = len(image_urls)
                    # Quit if enough images have been fetched
                    if len(image_urls) >= max_links_to_fetch:
                        break

                # Try to load more images
                load_more_button = wd.find_element_by_css_selector(".mye4qd")
                if load_more_button:
                    wd.execute_script("document.querySelector('.mye4qd').click();")
                    time.sleep(10) # wait for loading

                # Move the result start point further down
                results_start = len(thumbnail_results)

        return image_urls

    def _download(self, url: str, path: str) -> bool:
        """
        Download task
        :param url: image to download
        :param path: save location
        :return: bool, Download success/fail
        """
        try:
            wget.download(url, path, bar=None)
            return True
        except Exception:
            # Ignore download error
            return False

    def fetch_and_download_images(self, max_fetch_count: int, download_path: str) -> DownloadStat:
        """
        Performs the fetch and downloads tasks.
        ! This function is blocking !
        :param max_fetch_count: Maximum number of images to fetch
        :param download_path: Save location for the images
        :return DownloadStat, Download results
        """
        # Scrape the web for images, and obtain all the urls
        image_urls = self._fetch_image_urls(max_fetch_count)
        # Create unique download path
        image_download_path = os.path.join(download_path, self.query)
        os.makedirs(image_download_path, exist_ok=True)
        # Kick off image downloads
        with ProcessPoolExecutor(max_workers=_MAX_CONCURRENT_DOWNLOADS) as executor:
            jobs = [executor.submit(self._download, url, image_download_path)
                    for url in image_urls]
        # Download stats
        succeeded = 0
        failed = 0
        # Collect the download results
        for job in jobs:
            try:
                if job.result(timeout=_DOWNLOAD_TIMEOUT_SECONDS):
                    succeeded += 1
                else:
                    failed += 1
            except TimeoutError:
                failed += 1

        return DownloadStat(self.query, len(image_urls), succeeded, failed)
