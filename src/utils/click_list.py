# click_list.py
# Copyright (c) 2020 Ricky Dall'Armellina (rdall96). All Rights Reserved.

import click

class ClickListParamType(click.ParamType):
    name = "click_list"

    def convert(self, value, param, ctx):

        # Find the correct divider character
        strip_character: str
        if "," in value:
            strip_character = ","
        elif "\n" in value:
            strip_character = "\n"
        else:
            strip_character = " "

        # Convert
        try:
            return [x.strip().lower() for x in value.split(strip_character)]
        except Exception:
            self.fail(f"Expected comma separated list of strings, got {value!r}",
                      param, ctx)

CLICK_LIST = ClickListParamType()
