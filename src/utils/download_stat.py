# download_stat.py
# Copyright (c) 2020 Ricky Dall'Armellina (rdall96). All Rights Reserved.

class DownloadStat:

    def __init__(self, name: str, found: int, success: int, failed: int):
        """
        Download stat object. Contains info about a download session
        :param name: str, name for the download session
        :param found: int, number of images found on the web
        :param success: int, number of successful downloads
        :param failed: int, number of failed downloads
        """
        self.name = name
        self.found = found
        self.success = success
        self.failed = failed

    def formattedText(self) -> str:
        """
        Create a string of formatted text for the results
        :return: str
        """
        return f"{self.name.title()}\n\t" + "\n\t".join([
            f"Found: {self.found}",
            f"Downloaded: {self.success}",
            f"Failed: {self.failed}"
        ])
