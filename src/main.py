# main.py
# Copyright (c) 2020 Ricky Dall'Armellina (rdall96). All Rights Reserved.

import click
import os
from concurrent.futures import ProcessPoolExecutor

from utils.click_list import CLICK_LIST
from utils.image_downloader import ImageDownloader

_MAX_DOWNLOADERS: int = os.cpu_count()

def print_download_results(download_results: list):
    """
    Prints the download stats
    :param download_results: list, List of DownloadStat objects
    """
    print()
    print(" Image download results ".center(40, "~"))
    for item in download_results:
        print(f"\n{item.formattedText()}")
    print()

@click.command("ImageFetcher")
@click.option("-n", "--name", "image_names", type=CLICK_LIST, required=True,
              prompt="Image keywords (each keyword will be a new search)",
              help="Image keywords to use for the search. Each keyword is a new search with"
                   "a new batch download")
@click.option("-s", "--save-location", type=click.Path(), required=True,
              prompt="Path to save the downloaded images", help="Location to save images")
@click.option("-c", "--max-count", type=int, default=100,
              help="Max images to download per category")
def cli(image_names, save_location, max_count):
    """
    CLI entry point for ImageFetcher
    """
    print()
    print(" Image Fetcher ".center(80, "-"))

    # Get the output directory and create it
    save_path = (save_location, os.path.expanduser(save_location))["~" in save_location]
    os.makedirs(save_path, exist_ok=True)
    print(f"Created save location at: {save_path}")

    # Create a pool of image downloaders and assign an image query to each one
    with ProcessPoolExecutor(max_workers=_MAX_DOWNLOADERS) as executor:
        downloaders = [ImageDownloader(item) for item in image_names]
        # TODO: Put spinner for downloading images
        print("Downloading images...")
        jobs = [
            executor.submit(x.fetch_and_download_images, max_count, save_path)
            for x in downloaders
        ]
    # Wait for all jobs to complete and collect download stats
    print(f"Download complete, collecting results...")
    results = [job.result() for job in jobs]
    print_download_results(results)

    print(" DONE ".center(80, "-"))

    return 0

if __name__ == '__main__':
    exit(cli())
