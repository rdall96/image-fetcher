from setuptools import setup

setup(
    name='ImageFetcher',
    version='1.0',
    packages=['src'],
    setup_requires=['click', 'pillow', 'selenium', 'wget'],
    url='',
    license='',
    author="Ricky Dall'Armellina",
    author_email = 'rdall96@gmail.com',
    description = 'Search and download images from the web', install_requires=['webdriverplus']
)
